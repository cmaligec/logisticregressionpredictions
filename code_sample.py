# Extracted from a Jupyter Notebook assignment from a Data Science course.

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, roc_curve, precision_recall_curve, auc
import matplotlib.pyplot as plt

bank = pd.read_csv("Bank_A3.csv")
print(bank.loc[0:4])

print(bank.columns)
to_convert_cols = ['job', 'marital', 'education', 'day', 'month', 'poutcome']
bank = pd.get_dummies(bank, columns = to_convert_cols)
print(f'The size of the dataframe is: ' , bank.shape)

nrows, ncols = bank.shape
target = bank.target.values
count = 0
for item in target:
    if item == "yes":
        count +=1 # number of targets that are "yes"
accuracy = count / nrows
rounded_accuracy = np.around(accuracy, 3)
print(rounded_accuracy)

X = bank.drop('target', axis = 'columns').values
y = bank.target.values
Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, test_size=0.30, random_state=0)

X = bank.loc[:, ['age', 'default', 'balance']]
y = bank.target.values
Xtrain1, Xtest1, ytrain1, ytest1 = train_test_split(X, y, test_size=0.30, random_state=0)
banklr1 = LogisticRegression(penalty = 'none', class_weight = 'balanced').fit(Xtrain1, ytrain1)

print(f'The parameters of the model asociated to age, default and balance are respectively:', banklr1.coef_)

print(f'The intercept of the model is:', banklr1.intercept_)

## Put code here to compute criteria:
def compute_performance(yhat, y, classes):
    # First, get tp, tn, fp, fn
    tp = sum(np.logical_and(yhat == classes[1], y == classes[1]))
    tn = sum(np.logical_and(yhat == classes[0], y == classes[0]))
    fp = sum(np.logical_and(yhat == classes[1], y == classes[0]))
    fn = sum(np.logical_and(yhat == classes[0], y == classes[1]))

    print(f"tp: {tp} tn: {tn} fp: {fp} fn: {fn}")
    
    # Accuracy
    acc = (tp + tn) / (tp + tn + fp + fn)
        
    # Precision
    # Of the ones that I labelled + how many are actually +?
    precision = tp / (tp + fp) 
    
    # Recall
    # Of all the + in the data, how many do I correctly label?
    recall = tp / (tp + fn)
    
    # Sensitivity
    # Of all the + in the data, how many do I correctly label?
    sensitivity = recall
       
    
    # Specificity
    # Of all the - in the data, how many do I correctly label?
    specificity = tn / (tn + fp)
    
    # Print results
    print("Accuracy: ", round(acc, 3), "Precision: ", round(precision, 3), "Recall: ", round(recall, 3), "Sensitivity: ", round(sensitivity, 3), "Specificity: ", round(specificity, 3)) 
    

ytest_prob1 = banklr1.predict_proba(Xtest1)
yhat1 = banklr1.classes_[(ytest_prob1[:,1]>0.5).astype(int)]
ytest_hat1 = banklr1.predict(Xtest1)
compute_performance(ytest_hat1, ytest1, banklr1.classes_)

X = bank.drop('target', axis = 'columns').values
y = bank.target.values
Xtrain2, Xtest2, ytrain2, ytest2 = train_test_split(X, y, test_size=0.30, random_state=0)
banklr2 = LogisticRegression(class_weight = 'balanced', max_iter = 10000).fit(Xtrain2, ytrain2)
print(f'The parameters of the model are:', banklr2.coef_)
print(f'The intercept of the model is:', banklr2.intercept_)

ytest_prob2 = banklr2.predict_proba(Xtest2)
yhat2 = banklr2.classes_[(ytest_prob2[:,1]>0.5).astype(int)]
ytest_hat2 = banklr2.predict(Xtest2)
compute_performance(ytest_hat2, ytest2, banklr2.classes_)

sigmoid= lambda x: 1 / (1 + np.exp(-x))
z1 = np.dot(Xtest1, banklr1.coef_.T) + banklr1.intercept_
print(f'sigmoid probabilities using question 4 model:')
print(sigmoid(z1)[0:5])
z2 = np.dot(Xtest2, banklr2.coef_.T) + banklr2.intercept_
print(f'sigmoid probabilities using question 5 model:')
print(sigmoid(z2)[0:5])

# First 5 probabilities using question 4 model
banklr1.predict_proba(Xtest1)[0:5]

# First 5 probabilities using question 5 model
banklr2.predict_proba(Xtest2)[0:5]

# ROC for the simplier classifier
fpr1, tpr1, _ = roc_curve(ytest1, ytest_prob1[:, 1], pos_label = 'yes')
plt.plot(fpr1, tpr1)
print(auc(fpr1, tpr1))

# ROC for all-variable classifier
fpr2, tpr2, _ = roc_curve(ytest2, ytest_prob2[:, 1], pos_label = 'yes')
plt.plot(fpr2, tpr2)
print(auc(fpr2, tpr2))